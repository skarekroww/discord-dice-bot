package Commands;

import Objects.ICommand;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.entities.Message;

import java.util.List;

public class Clear implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent e) {
        boolean cleared = false;
        while(cleared == false) {
            List<Message> message = e.getMessage().getChannel().getHistory().retrievePast(100).complete();

            if(message.isEmpty()){
                cleared = true;
            }else{
                e.getMessage().getChannel().purgeMessages(message);
            }
        }
    }

    @Override
    public String getHelp() {
        return null;
    }

    @Override
    public String getInvoke() {
        return "clear";
    }
}
