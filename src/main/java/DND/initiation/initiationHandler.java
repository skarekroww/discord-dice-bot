package DND.initiation;
import Objects.ICommand;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.*;
import java.util.stream.Collectors;

public class initiationHandler implements ICommand {
    //these get initalized when the script is first ran, meaning anyone can access them.
    initiation init = new initiation();
    String setName;
    int setMod;
    static int userLocation;
    static HashMap<String, initiation> playerList = new HashMap();

    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent e) throws IndexOutOfBoundsException {
        Random random = new Random();
        String message = e.getMessage().getContentRaw();
        String user = String.valueOf(e.getMessage().getAuthor());


        try{
            String[] splitter = message.split("\\s", 3);
            String setter = splitter[2];
//            System.out.println(splitter[0]);
            //The command
//            System.out.println(splitter[1]);
            //The Name / modifier value
//            System.out.println(splitter[2]);


            if(!playerList.containsKey(user)){
                initiation newInit = new initiation(user, setName, setMod);
                playerList.put(user, newInit);
                System.out.println(user+" was added\n\n");
            }

            switch (splitter[1]){
                case "name":
                    init.setDndName(setter);
                    setName = setter;
                    System.out.println("setting " + user +" name to " + setter);
                    playerList.get(user).setDndName(setName);
                    break;

                case "mod":
                    init.setMod(Integer.parseInt(setter));
                    setMod = Integer.parseInt(setter);
                    playerList.get(user).setMod(setMod);
                    break;
                case "remove":break;
                default: break;
            }
            System.out.println(playerList.keySet());
            System.out.println(playerList.size());

            //checking to make sure everyone is being added properly and their names / mod change
//            for (Map.Entry<String, initiation> init:playerList.entrySet()) {
//                System.out.println(init.getKey() + " = " + init.getValue().getDndName() + " with a mod of " + init.getValue().getMod());
//            }

        }catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException){
            for (initiation init: playerList.values()) {
                int rolled = random.nextInt(20)+1;
//                System.out.println(init.getDndName() + " has rolled "
//                        + (init.getMod() + rolled));
                init.setDiceRoll(rolled+init.getMod());
            }

            for (initiation init: playerList.values()) {
                System.out.println(init.getDiscordName() + " has a roll value of: " + init.getDiceRoll());
            }

            List<String> sortedList = playerList.values().stream()
                    .sorted((x1, x2) -> compareInit(x1.getDiceRoll(), x2.getDiceRoll()))
                    .map(entry -> entry.getDndName() + " Rolled a "+entry.getDiceRoll()).collect(Collectors.toList());
            System.out.println("\n\n this should be sorted \n\n");
            sortedList.forEach(player -> System.out.println(player));

        }



        /*This is newer legacy code completely useless. Nulls out initNameChange and initSetMod.*/
        /*This was used in the legacy code Not needed*/
//    static HashMap<String, String> PCs = new HashMap();
//    static HashMap <String, Integer> initSet = new HashMap();
//    static HashMap hold = new HashMap();
//        try {
//            //gets author of the message this will become they key value of the PCs HashMap
//            String author = e.getAuthor().getName();
//            //gets someone's message
//            //this will also let us know which command to run, Name (name change, set, default)
//            String message = e.getMessage().getContentRaw();
//            //This command ties the author to their character name
//            //this matters so their character name will set to their character name
//            //a person can only be tied to one character
//            //TODO ** IF THEY CHANGE THEIR NAME THE initSet DOESN'T CHANGE THEIR NAMES**
//            if (e.getMessage().getContentRaw().split(" ")[1].equals("name")) {
//                initNameChange.main(author, hold, e, PCs);
//            }
//            //This will set their modifier for initiation
//            //Checks for .init set and if the person set their name
//            if (e.getMessage().getContentRaw().split(" ")[1].equals("set") && PCs.containsKey(author)) {
//                initSetMod.main(PCs.get(author), message, initSet, e);
//                System.out.println(initSet);
//
//            }
//            //does the oppisite of the above if(){} statement
//            //if they don't use a valid number but a string it gives them a mean message
//            else if (e.getMessage().getContentRaw().split(" ")[1].equals("set") && !PCs.containsKey(author)) {
//                e.getChannel().sendMessage("I need you to set your name, i'm retarded").queue();
//
//            }
//
//        } catch (ArrayIndexOutOfBoundsException err) {
//            Random random = new Random();
//            /*This streams the initSet to "roll" the dice for.
//            map explanation for myself:
//            x.getKey() has to be used, if i run x.getValues()+5 it removes the Key value
//            .map() basically changes how rolls will hold the information, and remove the entry set if not "stated"
//             */
//            List rolls = initSet.entrySet().stream().map(x -> x.getKey() + " rolled a " +
//                    (Math.round(Math.random() * 20) + 5)).collect(Collectors.toList());
//            System.out.println(rolls);
//            System.out.println(err);
//        }

//        All legacy code no longer useful
//        String init = e.getMessage().getContentRaw();
//        String[] split = init.split("\\s");
//
//
//        String enemies = null;
//
//        try {
//            enemies = split[1];
//        }catch (IndexOutOfBoundsException ex){
//
//        }
//        Random random = new Random();
//
//////This while loop just clears the chat, so all you see is the current initiation rolls to bypass confusion
//        boolean isWorking = true;
//        TextChannel channel = e.getGuild().getTextChannelsByName("initiation", true).get(0);
//        while (isWorking) {
//            List<Message> message = e.getGuild().getTextChannelsByName("initiation", true).get(0).getHistory().retrievePast(50).complete();
//            channel.purgeMessages(message);
//            if (message.isEmpty()) {
//                isWorking = false;
//            }
//        }
//        ArrayList<Roll> rolls = new ArrayList<>();
//
//        //This is super old code
////        Map<String, Integer> players = new HashMap<>();
////        players.put("Kroww", 5);
////        players.put("Mya", 0);
////        players.put("Firey", 1);
////        players.put("Jeff", 2);
////        players.put("Kasey", 3);
////        for (Map.Entry<String, Integer> entry : players.entrySet()) {
////            entry.setValue(entry.getValue() + random.nextInt(20) + 1);
////            System.out.println(entry.getKey() + " = " + entry.getValue());
////        }
////        e.getGuild().getTextChannelsByName("dice-rolling", true).get(0).sendMessage("Order is as follows").queue();
////
////
//////         Original way i used to cycle through the hashmap, if there were dup values it showed them multiple times
//
////
////
////        for (int i = 0; i < rolls.size(); i++) {
////            for (Map.Entry<String, Integer> entry : players.entrySet()) {
////                if (entry.getValue().equals(rolls.get(i))) {
////                    System.out.println(entry.getKey() + " got " + rolls.get(i));
////                    e.getGuild().getTextChannelsByName("initiation", true).get(0).sendMessage(entry.getKey() + " got a " + entry.getValue()).queue();
////                }
////            }
////            Collections.sort(rolls, Collections.reverseOrder());
////// NEW CODE
////            Iterator it = players.entrySet().iterator();
////            while (it.hasNext()) {
////                Map.Entry pair = (Map.Entry) it.next();
////                e.getGuild().getTextChannelsByName("initiation", true).get(0).sendMessage(pair.getKey() + " = " + pair.getValue()).queue();
////                it.remove();
////            }
//        //super old code ends here
//
//            //The final, we're done...no more coding here fuck off
//            rolls = new ArrayList<>();
//            rolls.add(new Roll("mya", random.nextInt(20) + 6));
//            rolls.add(new Roll("Krow", random.nextInt((20) + 1) + 6));
//            rolls.add(new Roll("firey", random.nextInt((20) + 1) + 1));
//            rolls.add(new Roll("Jeff", random.nextInt((20) + 1) + 0));
///*
// we want roll 1 and roll 2, we want it to RETURN roll 2 in place of roll 1
//-> shorthand for return
//Roll 2 is the larger number while Roll 1 is smaller
//Flip Roll 1 for Roll 2 and BOOM ascending order
// */
//            rolls.sort((Roll roll1, Roll roll2) -> roll2.roll.compareTo(roll1.roll));
//            String NiceRolls = rolls.stream().map((currentRoll) -> currentRoll.toString()).collect(Collectors.joining("\n"));
//            e.getGuild().getTextChannelsByName("initiation", true).get(0).sendMessage("Here's what we got \n" + NiceRolls).queue();

    }
    public static int compareInit(int player1, int player2){
        return player1 < player2 ? 1 : -1;
    }
    @Override
    public String getHelp () {
        return null;
    }

    @Override
    public String getInvoke () {
        return "init";
    }

}
