package DND.initiation;

public class initiation {
    String discordName;
    String dndName;
    int mod;
    int diceRoll;

    public int getDiceRoll() {
        return diceRoll;
    }

    public void setDiceRoll(int diceRoll) {
        this.diceRoll = diceRoll;
    }

    public initiation() {
        mod = 0;
        discordName = "";
        dndName = "UnNamed";
        return;
    }
    public initiation(String discordName, String dndName, int mod) {
        setDiscordName(discordName);
        setDndName(dndName);
        setMod(mod);
    }

    public initiation(String user, String setter) {
        setDiscordName(user);
        setDndName(setter);

    }

    public initiation(String user) {
        setDiscordName(user);
    }

    public String getDiscordName() {
        return discordName;
    }

    public void setDiscordName(String discordName) {
        this.discordName = discordName;
    }

    public String getDndName() {
        return dndName;
    }

    public void setDndName(String dndName) {
        this.dndName = dndName;
    }

    public int getMod() {
        return mod;
    }

    public void setMod(int mod) {
        this.mod = mod;
    }

    public void update(String discordName, String dndName, int mod){
        setDiscordName(discordName);
        setDndName(dndName);
        setMod(mod);
    }

    public String whosWho(){
        return (discordName+": " + dndName);
    }
}
