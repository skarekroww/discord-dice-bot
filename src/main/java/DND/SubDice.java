package DND;

import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.ArrayList;
import java.util.stream.Collectors;

import static DND.DiceHandler.random;

public class SubDice{
    public static void main(GuildMessageReceivedEvent e){
        String pin = e.getMessage().getAuthor().getName();
        ArrayList<AddingRoll.roll> holder = new ArrayList<>();
        String message = e.getMessage().getContentRaw();
        String numDNum = message.split("\\s")[1];
        String[] numbers = numDNum.split("d");
        String numberOfDice = numbers[0];

        //Damage now looks like X+Y and wont work, we have to go deeper
        String damage = numbers[1];
        //Splits damage
        String[] splittingToAdd = damage.split("\\-");
        //Damage is now the side of the dice (the x in x+y)
        damage = splittingToAdd[0];
        //Turns everything into an int so we can add it
        int numberOfDie = Integer.parseInt(numberOfDice);
        int dmg = Integer.parseInt(damage);
        int sub = Integer.parseInt(splittingToAdd[1]);
        int total = 0;

        e.getGuild().getTextChannelsByName("dice-rolling", true).get(0).sendMessage("_______________________________________________A new instance_______________________________________________").queue();
        e.getGuild().getTextChannelsByName("dice-rolling", true).get(0).sendMessage(pin + " needs " + numDNum).queue();

        //Rolls the numbers and adds the total and tells the total before showing the dice rolled
        for(int i = 0; i < numberOfDie; i++){

            int roll = random.nextInt(dmg)+1;

            holder.add(new AddingRoll.roll(roll, i+1));
            total += roll;
        }

        total -= sub;
        String inOne = holder.stream().map((currentRoll) -> currentRoll.toString()).collect(Collectors.joining("\n"));

        e.getGuild().getTextChannelsByName("dice-rolling", true).get(0).sendMessage(inOne + "\nyour total is " + total).queue();

    }
}
