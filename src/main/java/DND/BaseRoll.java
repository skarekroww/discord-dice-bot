package DND;

import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.ArrayList;
import java.util.stream.Collectors;

import static DND.DiceHandler.random;


public class BaseRoll {

    static class roll{
        public Integer die;
        public Integer roll;
        roll(Integer roll, Integer die){
            this.die = die;
            this.roll = roll;

        }
        @Override
        public String toString(){
            return "Die Number "+this.die + " rolled a " + roll;
        }
    }

    public static void GettingTotal(GuildMessageReceivedEvent e){

        String pin = e.getMessage().getAuthor().getName();
        ArrayList<roll> holder = new ArrayList();
        String message = e.getMessage().getContentRaw();
        String numDNum = message.split("\\s")[1];
        String[] numbers = numDNum.split("d");
        String numberOfDice = numbers[0];
        String damage = numbers[1];
        int numberOfDie = Integer.parseInt(numberOfDice);
        int dmg = Integer.parseInt(damage);
        int total = 0;
        e.getGuild().getTextChannelsByName("dice-rolling", true).get(0).sendMessage("_______________________________________________A new instance_______________________________________________ \n"+ pin + " needs " + numDNum).queue();
        for(int i = 0; i < numberOfDie; i++){

            int roll = random.nextInt(dmg) + 1;
            holder.add(new roll(roll, i+1));
            total += roll;
        }
        String inOne = holder.stream().map((allRolls) -> allRolls.toString()).collect(Collectors.joining("\n"));
        e.getGuild().getTextChannelsByName("dice-rolling", true).get(0).sendMessage(inOne + "\n for a total of "+ total).queue();
        e.getGuild().getTextChannelsByName("dice-rolling", true).get(0).sendMessage("_______________________________________________End of instance_______________________________________________").queue();
    }
}