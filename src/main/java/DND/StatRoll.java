package DND;

import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.ArrayList;

import static DND.DiceHandler.random;

public class StatRoll {

    static class roll{
        public Integer roll;
        public Integer die;
        roll(Integer die, Integer roll){
            this.die = die;
            this.roll = roll;
        }
        @Override
        public String toString(){
            return "Die Number "+ die + " Rolled a "+ roll;
        }
    }

    public static void main(GuildMessageReceivedEvent e){
        String pin = e.getMessage().getAuthor().getName();
        ArrayList<roll> holder = new ArrayList<>();
        ArrayList<roll> removed = new ArrayList<>();
        String message = e.getMessage().getContentRaw();
        String numDNum = message.split("\\s")[1];
        String[] numbers = numDNum.split("d");
        String numberOfDice = numbers[0];
        String damage = numbers[1];
        String dropString = numbers[2];
        int drop = Integer.parseInt(dropString);

        int numberOfDie = Integer.parseInt(numberOfDice);
        int dmg = Integer.parseInt(damage);
        int total = 0;

        e.getGuild().getTextChannelsByName("dice-rolling", true).get(0).sendMessage("_______________________________________________A new instance_______________________________________________ \n" + pin + " needs " + numDNum).queue();


        for(int i = 0; i < numberOfDie; i++){

            int roll = random.nextInt(dmg)+1;

            holder.add(new roll(i+1, roll));
            total += roll;

        }
        //roll is the name of the class
        holder.sort((roll roll1, roll roll2) -> roll1.roll.compareTo(roll2.roll));

        //TODO while statement to delete dropString
        for(int x = 0; x< drop; x++) {
//            System.out.println("lowest die are " + holder.get(x));
//            holder.remove(holder.get(x));

        }
//        System.out.println(removed.toString());
//        String inOne = removed.stream().map((allremoved) -> allremoved.toString()).collect(Collectors.joining("\n"));
//        e.getGuild().getTextChannelsByName("dice-rolling",true).get(0).sendMessage("Im removing "+ inOne).queue();
//        inOne = holder.stream().map((allrolls) -> allrolls.toString()).collect(Collectors.joining("\n"));
//
//        System.out.println(inOne);
//
//
//        e.getGuild().getTextChannelsByName("dice-rolling", true).get(0).sendMessage(inOne).queue();
//
//        e.getGuild().getTextChannelsByName("dice-rolling", true).get(0).sendMessage("_______________________________________________End of instance_______________________________________________").queue();
    }
}
