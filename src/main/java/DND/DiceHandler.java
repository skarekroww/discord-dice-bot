package DND;

import Objects.ICommand;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;
import java.util.Random;

public class DiceHandler implements ICommand {

    public static Random random = new Random();


    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent e) {


        String message = e.getMessage().getContentRaw();
        String numDNum = message.split("\\s")[1];
        String[] numbers = numDNum.split("d");

        String damage = null;

        try {
            damage = numbers[2];
        }catch (IndexOutOfBoundsException ex){

        }



        if(e.getMessage().getContentRaw().contains("+")){
            AddingRoll.main(e);
        }else if(e.getMessage().getContentRaw().contains("-")){
            SubDice.main(e);
        }
        else if(damage == null){
            BaseRoll.GettingTotal(e);
        }

        else if (damage != null){
            StatRoll.main(e);
        }else{
        }


    }

    @Override
    public String getHelp() {
        return null;
    }

    @Override
    public String getInvoke() {
        return "roll";
    }
}
