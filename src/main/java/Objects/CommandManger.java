package Objects;

//imports the commands package

import Commands.*;
import DND.DiceHandler;
import DND.initiation.initiationHandler;
import Jackson.goo;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

//imports the presence package within commands package

class CommandManger {
    private final Map<String, ICommand> commands = new HashMap<>();

    CommandManger()
    {

        addCommand(new DiceHandler());
        addCommand(new Credits());
        addCommand(new initiationHandler());
        addCommand(new Clear());
        addCommand(new goo());



    }
    private void addCommand(ICommand command) {
        //if the hashmap commands does not contain the getInvoke word of the above ^^ classes it adds them into the hashmap
        //manually put them into the map buy doing commands.put(new CommandName().getInvoke, new CommandName()) The handler command will pull the .Handle
        if (!commands.containsKey(command.getInvoke())) {
            commands.put(command.getInvoke(), command);
        }
    }

    void handleCommand(GuildMessageReceivedEvent event) {
        //splits the message
        final String[] split = event.getMessage().getContentRaw().replaceFirst(
                Pattern.quote(Constants.PREFIX), "").split("\\s+");
        //if the first word contains the prefix (!) removes it and splits the code

        final String invoke = split[0].toLowerCase();

        //checks to see if the Hashmap contains the split message above (also known as invoke)
        if(commands.containsKey(invoke)){

            final List<String> args = Arrays.asList(split).subList(1,split.length) ;
            commands.get(invoke).handle(args,event);
        }
    }
}
