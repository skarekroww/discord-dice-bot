**[What is Discord?](https://discord.com/safety/360044149331-what-is-discord)**<br>
**[What is a Discord Bot?](https://docs.statbot.net/docs/guide/bot/)**

My discord bot is intended to be a chat bot and act as a pseudo dice roller and pull images from specific websites. The bot has multiple commands all of which are meant to act as a dice roll. The multiple methods are based on if the user needs to add a value to the dice roll, drop a set number of dice from the dice roll, or remove a set number of dice (the lowest rolled) from a dice roll. An example of a dice roll command input would be “.roll 4d6”. ‘.’ Is the constant prefix allowing the listener to know a command is being set, roll is the command  and after the split allows the bot to know what to “roll” in this example four six sided dice are to be “roll”.<br><br>
The original goal of making this bot was for educational perposes. With the limited knowledge of java I had i set out to make something very complax at my level for the time. This bot has tought me much about working with and creating objects, Maven, depeninces, Gitlab, and APIs both working with them and reading them. My favorite script in all of this is [`.roll`](https://gitlab.com/skarekroww/discord-dice-bot/-/blob/main/src/main/java/Objects/Listener.java#L110) linked is the original dice code. The reason this is my favorite is because of how far it's come. Originally I didn't know about `.split()` and had to figure out a way around that was this code. It also marks when I met whom i'd consider my programming mentor.

**How To Build**<br>
In order to use this application, you have to have your own discord bot token which you can create [here](https://discord.com/developers/applications). 
After the Token is created under [Objects in the App Class](https://gitlab.com/skarekroww/discord-dice-bot/-/blob/main/src/main/java/Objects/App.java#L23) place your own token in line 23.
After that you just need to invite the bot to your discord and run the application to turn it on. 

**Commands**<br>
.roll XdY `.roll 1d20` – Will generate an array with the max size of X and the max value of Y and prints out the values of the array.<br>
.roll XdYrZ  `.roll 4d6r1`– Will do the same as about but will reroll numbers below the Z value.<br>
.roll XdY +- Z `.roll 1d20+5` – This will make the bot either add or subtract Z to the array total.<br>
.init – This will generate a number max of 20 for the specified players.<br>

**Used Libraries**<br>
[Discord JDA](https://github.com/DV8FromTheWorld/JDA) - Access the Discord bot<br>
~~[JSoup](https://github.com/jhy/jsoup/)  - Access websites and reads the HTML<br>~~
~~[Jackson](https://github.com/FasterXML/jackson) - Access JSON and XML to turn into POJOs<br>~~
